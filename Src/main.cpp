/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <ros.h>
#include <ros/ros_time.h>
#include <std_msgs/String.h>
#include <std_msgs/UInt32.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Int8.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Twist.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;

UART_HandleTypeDef huart1;
DMA_HandleTypeDef hdma_usart1_rx;
DMA_HandleTypeDef hdma_usart1_tx;

/* USER CODE BEGIN PV */
Encoder leftEncoder;
Encoder rightEncoder;

Motor leftMotor;
Motor rightMotor;

PIDController leftPid;
PIDController rightPid;

bool enablePidControl = true;

bool publishDebugInfo = false; //publish encoder values and measured speeds

Position robotPosition;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM4_Init(void);
/* USER CODE BEGIN PFP */
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
ros::NodeHandle nh;

void setPWMLeftCb( const std_msgs::UInt32& cmd_msg){ setPWMLeftMotor(cmd_msg.data); }
ros::Subscriber<std_msgs::UInt32> subPwmLeft("/left_motor/pwm", setPWMLeftCb);
void setPWMRightCb( const std_msgs::UInt32& cmd_msg){ setPWMRightMotor(cmd_msg.data); }
ros::Subscriber<std_msgs::UInt32> subPwmRight("/right_motor/pwm", setPWMRightCb);

void setDirLeftCb( const std_msgs::Bool& cmd_msg) {setDirLeftMotor(cmd_msg.data);}
ros::Subscriber<std_msgs::Bool> subDirLeft("/left_motor/dir", setDirLeftCb);
void setDirRightCb( const std_msgs::Bool& cmd_msg) {setDirRightMotor(cmd_msg.data);}
ros::Subscriber<std_msgs::Bool> subDirRight("/right_motor/dir", setDirRightCb);

void setSpeedRightCb( const std_msgs::Float32& cmd_msg){rightMotor.setSpeed = float(cmd_msg.data)*1000;}
ros::Subscriber<std_msgs::Float32> subRightSetSpeed("/right_motor/set_speed", setSpeedRightCb);
void setSpeedLeftCb( const std_msgs::Float32& cmd_msg){leftMotor.setSpeed = float(cmd_msg.data)*1000;}
ros::Subscriber<std_msgs::Float32> subLeftSetSpeed("/left_motor/set_speed", setSpeedLeftCb);

void leftPidCb( const geometry_msgs::Vector3& cmd_msg){leftPid.P = cmd_msg.x; leftPid.I = cmd_msg.y; leftPid.D = cmd_msg.z;}
ros::Subscriber<geometry_msgs::Vector3> leftPidSub("/left_motor/pid", leftPidCb);
void rightPidCb( const geometry_msgs::Vector3& cmd_msg){rightPid.P = cmd_msg.x; rightPid.I = cmd_msg.y; rightPid.D = cmd_msg.z;}
ros::Subscriber<geometry_msgs::Vector3> rightPidSub("/right_motor/pid", rightPidCb);
void enablePidCb( const std_msgs::Bool& cmd_msg){enablePidControl=cmd_msg.data;}
ros::Subscriber<std_msgs::Bool> enablePidSub("/enable_pid", enablePidCb);

uint16_t lastVelMsgTickCount = 0;

void robotSpeedCb( const geometry_msgs::Twist& cmd_msg)
{
	lastVelMsgTickCount = 0;
	rightMotor.setSpeed = (cmd_msg.linear.x + HALF_WHEEL_DISTANCE * cmd_msg.angular.z)*1000;
	leftMotor.setSpeed = (cmd_msg.linear.x - HALF_WHEEL_DISTANCE * cmd_msg.angular.z)*1000;
}
ros::Subscriber<geometry_msgs::Twist> robotSpeedSub("/cmd_vel", robotSpeedCb);

void resetMotors()
{
	setPWMLeftMotor(0);
	leftMotor.setSpeed = 0;
	leftMotor.pwm = 0;

	setPWMRightMotor(0);
	rightMotor.setSpeed = 0;
	rightMotor.pwm = 0;
}

void publishSpeed();

std_msgs::Int32 leftEncVal;
ros::Publisher leftEncoderPub("/left_motor/encoder", &leftEncVal);
std_msgs::Int32 rightEncVal;
ros::Publisher rightEncoderPub("/right_motor/encoder", &rightEncVal);

std_msgs::Float32 leftSpeedVal;
ros::Publisher leftSpeedPub("/left_motor/speed", &leftSpeedVal);
std_msgs::Float32 rightSpeedVal;
ros::Publisher rightSpeedPub("/right_motor/speed", &rightSpeedVal);

geometry_msgs::TransformStamped odom;
tf::TransformBroadcaster odomBroadcaster;
char baseLinkFrame[] = "/base_link";
char odomFrame[] = "/odom";


void publishOdom()
{
  float leftAngle = (float(leftEncoder.odomTicks)/400.)*2.*M_PI;
  float rightAngle = (float(rightEncoder.odomTicks)/400.)*2.*M_PI;
  robotPosition.x += (WHEEL_RADIUS/2.)*(leftAngle+rightAngle)*cos(robotPosition.yaw);
  robotPosition.y += (WHEEL_RADIUS/2.)*(leftAngle+rightAngle)*sin(robotPosition.yaw);
  robotPosition.yaw += (WHEEL_RADIUS/(2.*HALF_WHEEL_DISTANCE))*(rightAngle-leftAngle);

  odom.transform.translation.x = robotPosition.x;
  odom.transform.translation.y = robotPosition.y;
  odom.transform.translation.z = 0.0;
  odom.transform.rotation = tf::createQuaternionFromYaw(robotPosition.yaw);
  odom.header.stamp = nh.now();
  odomBroadcaster.sendTransform(odom);

  if(publishDebugInfo)
  {
	  leftEncVal.data = leftEncoder.odomTicks;
	  leftEncoderPub.publish(&leftEncVal);
	  rightEncVal.data = rightEncoder.odomTicks;
	  rightEncoderPub.publish(&rightEncVal);
  }


  leftEncoder.odomTicks = 0;
  rightEncoder.odomTicks = 0;
 }
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  /*
   * 50 Hz frequnecy - for lowest speed i get 2,3 pidControllerTicks per interrupt - a little low
   * 25 Hz frequnecy - for lowest speed i get 6,7 pidControllerTicks per interrupt
   */
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART1_UART_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  /* USER CODE BEGIN 2 */
  HAL_TIM_Base_Start_IT(&htim4);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  setPWMLeftMotor(0);
  setPWMRightMotor(0);

  leftPid.P = 2000;
  leftPid.I = 0;
  leftPid.D = 0;

  rightPid.P = 2000;
  rightPid.I = 0;
  rightPid.D = 0;

  robotPosition.x = 0;
  robotPosition.y = 0;
  robotPosition.yaw = 0;

  leftMotor.measuredSpeed = 0;
  leftMotor.setSpeed = 0;
  leftMotor.pwm = 0;

  rightMotor.measuredSpeed = 0;
  rightMotor.setSpeed = 0;
  rightMotor.pwm = 0;

	rightEncoder.pidControllerTicks = 0;
	rightEncoder.odomTicks = 0;

	leftEncoder.pidControllerTicks = 0;
	leftEncoder.odomTicks = 0;

  nh.initNode();

  nh.subscribe(subPwmLeft);
  nh.subscribe(subPwmRight);

  nh.subscribe(subDirLeft);
  nh.subscribe(subDirRight);

  nh.subscribe(subLeftSetSpeed);
  nh.subscribe(subRightSetSpeed);
  
  nh.subscribe(leftPidSub);
  nh.subscribe(rightPidSub);
  nh.subscribe(enablePidSub);
  

  nh.subscribe(robotSpeedSub);

  if(publishDebugInfo)
  {
	  nh.advertise(leftEncoderPub);
	  nh.advertise(rightEncoderPub);
	  nh.advertise(leftSpeedPub);
	  nh.advertise(rightSpeedPub);
  }

  odom.header.frame_id = odomFrame;
  odom.child_frame_id = baseLinkFrame;
  odomBroadcaster.init(nh);

  int publishing_interval = 50; //20Hz
  int publishing_last = HAL_GetTick();

  uint8_t cmd_vel_timeout_interval = 10; //100Hz
  uint32_t cmd_vel_timeout_last = HAL_GetTick();


  while (1)
  {
    if (nh.connected())
    {
      if(HAL_GetTick() - cmd_vel_timeout_last > cmd_vel_timeout_interval)
      {
    	  cmd_vel_timeout_last = HAL_GetTick();
    	  if(lastVelMsgTickCount > 39) //2.5 Hz
    		  resetMotors();

    	  ++lastVelMsgTickCount;
      }

      if(HAL_GetTick() - publishing_last > publishing_interval)
      {
        publishing_last = HAL_GetTick();

        nh.spinOnce();
        publishOdom();
        if(publishDebugInfo)
        {
        	nh.spinOnce();
        	publishSpeed();
        }
        HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);

      }

    }
    else
    {
      resetMotors();
      HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);
    }

    nh.spinOnce();
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 12;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 60000;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */
  HAL_TIM_MspPostInit(&htim3);

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 24;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 59999;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 57600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel4_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel4_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel4_IRQn);
  /* DMA1_Channel5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel5_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, MOTOR1_DIR_Pin|MOTOR2_DIR_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : Encoder1_A_Pin Encoder2_A_Pin */
  GPIO_InitStruct.Pin = Encoder1_A_Pin|Encoder2_A_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : Encoder1_B_Pin Encoder2_B_Pin */
  GPIO_InitStruct.Pin = Encoder1_B_Pin|Encoder2_B_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : MOTOR1_DIR_Pin MOTOR2_DIR_Pin */
  GPIO_InitStruct.Pin = MOTOR1_DIR_Pin|MOTOR2_DIR_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

  HAL_NVIC_SetPriority(EXTI1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI1_IRQn);

  HAL_NVIC_SetPriority(EXTI4_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI4_IRQn);

  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

}

/* USER CODE BEGIN 4 */
void setPWMLeftMotor(uint32_t value)
{
  TIM_OC_InitTypeDef sConfigOC;

  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = value;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1);
  HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
}

void setPWMRightMotor(uint32_t value)
{
  TIM_OC_InitTypeDef sConfigOC;

  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = value;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2);
  HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
}

void setDirLeftMotor(bool value)
{
  //False - reverse
	//True - forward
	if(value)
		HAL_GPIO_WritePin(MOTOR1_DIR_GPIO_Port, MOTOR1_DIR_Pin, GPIO_PIN_SET);
	else
		HAL_GPIO_WritePin(MOTOR1_DIR_GPIO_Port, MOTOR1_DIR_Pin, GPIO_PIN_RESET);
}
void setDirRightMotor(bool value)
{
	//False - reverse
	//True - forward
	if(value)
		HAL_GPIO_WritePin(MOTOR2_DIR_GPIO_Port, MOTOR2_DIR_Pin, GPIO_PIN_SET);
	else
		HAL_GPIO_WritePin(MOTOR2_DIR_GPIO_Port, MOTOR2_DIR_Pin, GPIO_PIN_RESET);
}

void publishSpeed()
{
	leftSpeedVal.data = leftMotor.measuredSpeed;
	leftSpeedPub.publish(&leftSpeedVal);

	rightSpeedVal.data = rightMotor.measuredSpeed;
	rightSpeedPub.publish(&rightSpeedVal);
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
