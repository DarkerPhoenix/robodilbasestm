/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdbool.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
typedef struct
{
	int16_t pidControllerTicks;
	int16_t odomTicks;
} Encoder;

typedef struct
{
	int16_t measuredSpeed;
	int16_t setSpeed;
	int32_t pwm;
} Motor;

typedef struct
{
	int16_t P;
	int16_t I;
	int16_t D;
} PIDController;

typedef struct
{
	float x;
	float y;
	float yaw;
} Position;
/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
#define WHEEL_RADIUS 0.145
#define WHEEL_RADIUS_MM 145
#define PI 3.14159265
#define PID_FREQUENCY 24
#define MAX_PWM_VAL 20000 //Part of the real maximum value - safety measure to disable full speed on motors
#define MIN_PWM_VAL_LEFT 14500
#define MIN_PWM_VAL_RIGHT 16000
#define MIN_SPEED_MM 50
#define HALF_WHEEL_DISTANCE 0.2535
/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
void setPWMLeftMotor(uint32_t value);
void setPWMRightMotor(uint32_t value);
void setDirLeftMotor(bool value);
void setDirRightMotor(bool value);
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define Encoder1_A_Pin GPIO_PIN_4
#define Encoder1_A_GPIO_Port GPIOA
#define Encoder1_A_EXTI_IRQn EXTI4_IRQn
#define Encoder2_A_Pin GPIO_PIN_5
#define Encoder2_A_GPIO_Port GPIOA
#define Encoder2_A_EXTI_IRQn EXTI9_5_IRQn
#define MOTOR1_PWM_Pin GPIO_PIN_6
#define MOTOR1_PWM_GPIO_Port GPIOA
#define MOTOR2_PWM_Pin GPIO_PIN_7
#define MOTOR2_PWM_GPIO_Port GPIOA
#define Encoder1_B_Pin GPIO_PIN_0
#define Encoder1_B_GPIO_Port GPIOB
#define Encoder1_B_EXTI_IRQn EXTI0_IRQn
#define Encoder2_B_Pin GPIO_PIN_1
#define Encoder2_B_GPIO_Port GPIOB
#define Encoder2_B_EXTI_IRQn EXTI1_IRQn
#define MOTOR1_DIR_Pin GPIO_PIN_10
#define MOTOR1_DIR_GPIO_Port GPIOB
#define MOTOR2_DIR_Pin GPIO_PIN_11
#define MOTOR2_DIR_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
