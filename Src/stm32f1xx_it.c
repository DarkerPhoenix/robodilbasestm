/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32f1xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdbool.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern TIM_HandleTypeDef htim4;
extern DMA_HandleTypeDef hdma_usart1_rx;
extern DMA_HandleTypeDef hdma_usart1_tx;
extern UART_HandleTypeDef huart1;
/* USER CODE BEGIN EV */
extern Encoder leftEncoder;
extern Encoder rightEncoder;

extern Motor leftMotor;
extern Motor rightMotor;

extern PIDController leftPid;
extern PIDController rightPid;
extern bool enablePidControl;
/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M3 Processor Interruption and Exception Handlers          */ 
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */

  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Memory management fault.
  */
void MemManage_Handler(void)
{
  /* USER CODE BEGIN MemoryManagement_IRQn 0 */

  /* USER CODE END MemoryManagement_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_MemoryManagement_IRQn 0 */
    /* USER CODE END W1_MemoryManagement_IRQn 0 */
  }
}

/**
  * @brief This function handles Prefetch fault, memory access fault.
  */
void BusFault_Handler(void)
{
  /* USER CODE BEGIN BusFault_IRQn 0 */

  /* USER CODE END BusFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_BusFault_IRQn 0 */
    /* USER CODE END W1_BusFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Undefined instruction or illegal state.
  */
void UsageFault_Handler(void)
{
  /* USER CODE BEGIN UsageFault_IRQn 0 */

  /* USER CODE END UsageFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_UsageFault_IRQn 0 */
    /* USER CODE END W1_UsageFault_IRQn 0 */
  }
}

/**
  * @brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{
  /* USER CODE BEGIN SVCall_IRQn 0 */

  /* USER CODE END SVCall_IRQn 0 */
  /* USER CODE BEGIN SVCall_IRQn 1 */

  /* USER CODE END SVCall_IRQn 1 */
}

/**
  * @brief This function handles Debug monitor.
  */
void DebugMon_Handler(void)
{
  /* USER CODE BEGIN DebugMonitor_IRQn 0 */

  /* USER CODE END DebugMonitor_IRQn 0 */
  /* USER CODE BEGIN DebugMonitor_IRQn 1 */

  /* USER CODE END DebugMonitor_IRQn 1 */
}

/**
  * @brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{
  /* USER CODE BEGIN PendSV_IRQn 0 */

  /* USER CODE END PendSV_IRQn 0 */
  /* USER CODE BEGIN PendSV_IRQn 1 */

  /* USER CODE END PendSV_IRQn 1 */
}

/**
  * @brief This function handles System tick timer.
  */
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32F1xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f1xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles EXTI line0 interrupt.
  */
void EXTI0_IRQHandler(void)
{
  /* USER CODE BEGIN EXTI0_IRQn 0 */

  if(HAL_GPIO_ReadPin(Encoder1_B_GPIO_Port, Encoder1_B_Pin)) //Rising edge
  {
    if (HAL_GPIO_ReadPin(Encoder1_A_GPIO_Port, Encoder1_A_Pin))
    {
      --rightEncoder.pidControllerTicks;
      --rightEncoder.odomTicks;
    }
    else
    {
      ++rightEncoder.pidControllerTicks;
      ++rightEncoder.odomTicks;
    }
  }
  else // Falling edge
  {
    if (HAL_GPIO_ReadPin(Encoder1_A_GPIO_Port, Encoder1_A_Pin))
    {
      ++rightEncoder.pidControllerTicks;
      ++rightEncoder.odomTicks;
    }
    else
    {
      --rightEncoder.pidControllerTicks;
      --rightEncoder.odomTicks;
    }
  }
  
  /* USER CODE END EXTI0_IRQn 0 */
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
  /* USER CODE BEGIN EXTI0_IRQn 1 */

  /* USER CODE END EXTI0_IRQn 1 */
}

/**
  * @brief This function handles EXTI line1 interrupt.
  */
void EXTI1_IRQHandler(void)
{
  /* USER CODE BEGIN EXTI1_IRQn 0 */

  if(HAL_GPIO_ReadPin(Encoder2_B_GPIO_Port, Encoder2_B_Pin)) //Rising edge
  {
    if (HAL_GPIO_ReadPin(Encoder2_A_GPIO_Port, Encoder2_A_Pin))
    {
      ++leftEncoder.pidControllerTicks;
      ++leftEncoder.odomTicks;
    }
    else
    {
      --leftEncoder.pidControllerTicks;
      --leftEncoder.odomTicks;
    }
  }
  else // Falling edge
  {
    if (HAL_GPIO_ReadPin(Encoder2_A_GPIO_Port, Encoder2_A_Pin))
    {
      --leftEncoder.pidControllerTicks;
      --leftEncoder.odomTicks;
    }
    else
    {
      ++leftEncoder.pidControllerTicks;
      ++leftEncoder.odomTicks;
    }
  }
  
  /* USER CODE END EXTI1_IRQn 0 */
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
  /* USER CODE BEGIN EXTI1_IRQn 1 */

  /* USER CODE END EXTI1_IRQn 1 */
}

/**
  * @brief This function handles EXTI line4 interrupt.
  */
void EXTI4_IRQHandler(void)
{
  /* USER CODE BEGIN EXTI4_IRQn 0 */
  if(HAL_GPIO_ReadPin(Encoder1_A_GPIO_Port, Encoder1_A_Pin)) //Rising edge
  {
    if (HAL_GPIO_ReadPin(Encoder1_B_GPIO_Port, Encoder1_B_Pin))
    {
      ++rightEncoder.pidControllerTicks;
      ++rightEncoder.odomTicks;
    }
    else
    {
      --rightEncoder.pidControllerTicks;
      --rightEncoder.odomTicks;
    }
  }
  else // Falling edge
  {
    if (HAL_GPIO_ReadPin(Encoder1_B_GPIO_Port, Encoder1_B_Pin))
    {
      --rightEncoder.pidControllerTicks;
      --rightEncoder.odomTicks;
    }
    else
    {
      ++rightEncoder.pidControllerTicks;
      ++rightEncoder.odomTicks;
    }
  }
  
  /* USER CODE END EXTI4_IRQn 0 */
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_4);
  /* USER CODE BEGIN EXTI4_IRQn 1 */

  /* USER CODE END EXTI4_IRQn 1 */
}

/**
  * @brief This function handles DMA1 channel4 global interrupt.
  */
void DMA1_Channel4_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Channel4_IRQn 0 */

  /* USER CODE END DMA1_Channel4_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_usart1_tx);
  /* USER CODE BEGIN DMA1_Channel4_IRQn 1 */

  /* USER CODE END DMA1_Channel4_IRQn 1 */
}

/**
  * @brief This function handles DMA1 channel5 global interrupt.
  */
void DMA1_Channel5_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Channel5_IRQn 0 */

  /* USER CODE END DMA1_Channel5_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_usart1_rx);
  /* USER CODE BEGIN DMA1_Channel5_IRQn 1 */

  /* USER CODE END DMA1_Channel5_IRQn 1 */
}

/**
  * @brief This function handles EXTI line[9:5] interrupts.
  */
void EXTI9_5_IRQHandler(void)
{
  /* USER CODE BEGIN EXTI9_5_IRQn 0 */
  
  if(HAL_GPIO_ReadPin(Encoder2_A_GPIO_Port, Encoder2_A_Pin)) //Rising edge
  {
    if (HAL_GPIO_ReadPin(Encoder2_B_GPIO_Port, Encoder2_B_Pin))
    {
      --leftEncoder.pidControllerTicks;
      --leftEncoder.odomTicks;
    }
    else
    {
      ++leftEncoder.pidControllerTicks;
      ++leftEncoder.odomTicks;
    }
  }
  else // Falling edge
  {
    if (HAL_GPIO_ReadPin(Encoder2_B_GPIO_Port, Encoder2_B_Pin))
    {
      ++leftEncoder.pidControllerTicks;
      ++leftEncoder.odomTicks;
    }
    else
    {
      --leftEncoder.pidControllerTicks;
      --leftEncoder.odomTicks;
    }
  }
  

  /* USER CODE END EXTI9_5_IRQn 0 */
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_5);
  /* USER CODE BEGIN EXTI9_5_IRQn 1 */

  /* USER CODE END EXTI9_5_IRQn 1 */
}

/**
  * @brief This function handles TIM4 global interrupt.
  */
void TIM4_IRQHandler(void)
{
  /* USER CODE BEGIN TIM4_IRQn 0 */
	leftMotor.measuredSpeed = ((leftEncoder.pidControllerTicks*2*WHEEL_RADIUS_MM*PID_FREQUENCY)*PI)/400; // mm/s
	rightMotor.measuredSpeed = ((rightEncoder.pidControllerTicks*2*WHEEL_RADIUS_MM*PID_FREQUENCY)*PI)/400; // mm/s

	leftEncoder.pidControllerTicks = 0;
	rightEncoder.pidControllerTicks = 0;

	if(enablePidControl)
	{
		if (abs(leftMotor.pwm) < MIN_PWM_VAL_LEFT)
		{
			if (abs(leftMotor.setSpeed)<MIN_SPEED_MM)
				leftMotor.pwm = 0;
			else if (leftMotor.setSpeed > 0)
				leftMotor.pwm = MIN_PWM_VAL_LEFT;
		    else
		    	leftMotor.pwm = -MIN_PWM_VAL_LEFT;
		 }
		int32_t leftPwm = leftMotor.pwm + (leftPid.P*(leftMotor.setSpeed - leftMotor.measuredSpeed))/1000;

		if(leftPwm<-MAX_PWM_VAL)
		  leftPwm = -MAX_PWM_VAL;
		else if (leftPwm>MAX_PWM_VAL)
		  leftPwm = MAX_PWM_VAL;


		if (abs(leftMotor.setSpeed)<MIN_SPEED_MM)
			leftPwm = 0;

		leftMotor.pwm = leftPwm;

		if (abs(rightMotor.pwm) < MIN_PWM_VAL_RIGHT)
		{
			if (abs(rightMotor.setSpeed)<MIN_SPEED_MM)
				rightMotor.pwm = 0;
		   	else if (rightMotor.setSpeed > 0)
		   		rightMotor.pwm = MIN_PWM_VAL_RIGHT;
	    	else
	    		rightMotor.pwm = -MIN_PWM_VAL_RIGHT;
	    }
		int32_t rightPwm = rightMotor.pwm + (rightPid.P*(rightMotor.setSpeed - rightMotor.measuredSpeed))/1000;

		if(rightPwm<-MAX_PWM_VAL)
		  rightPwm = -MAX_PWM_VAL;
		else if (rightPwm>MAX_PWM_VAL)
		  rightPwm = MAX_PWM_VAL;

		if (abs(rightMotor.setSpeed)<MIN_SPEED_MM)
			rightPwm = 0;

		rightMotor.pwm = rightPwm;


    //Send PWM and Dir values
		if(leftPwm<0)
		{
		  setDirLeftMotor(true);
		  setPWMLeftMotor(abs(leftPwm));

		}
		else
		{
		  setDirLeftMotor(false);
		  setPWMLeftMotor(leftPwm);
		}

    if(rightPwm<0)
		{
		  setDirRightMotor(true);
		  setPWMRightMotor(abs(rightPwm));
		}
		else
		{
		  setDirRightMotor(false);
		  setPWMRightMotor(rightPwm);

		}
	}
  /* USER CODE END TIM4_IRQn 0 */
  HAL_TIM_IRQHandler(&htim4);
  /* USER CODE BEGIN TIM4_IRQn 1 */

  /* USER CODE END TIM4_IRQn 1 */
}

/**
  * @brief This function handles USART1 global interrupt.
  */
void USART1_IRQHandler(void)
{
  /* USER CODE BEGIN USART1_IRQn 0 */

  /* USER CODE END USART1_IRQn 0 */
  HAL_UART_IRQHandler(&huart1);
  /* USER CODE BEGIN USART1_IRQn 1 */

  /* USER CODE END USART1_IRQn 1 */
}

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
